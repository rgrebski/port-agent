# Port Agent

Port Agent app.

## Running the application

Run `./mvnw`

## API
`/api/health` - REST Endopint for checking health. Returns:  
- 503 - when monitored app is down
- 200 - when monitored app is down

This endpoint also returns body with status details, for example:  

```
{
  "healthy": false,
  "usedPorts": [
    3004,
    3003,
    3002,
    3001,
    3000
  ]
}
```

## Dashboard
Open http://localhost:8080 to see status dashbord

## Logs
Debug logs are enabled for `com.grebski.sample` in order to see what is happening under the hood 

## Taken Port simulator

After startup this app starts opening ports, starting from 3000 and takes next ports (3001, 3002, ..).  
This is only to simulate scenario.  
In order to disable the simulator please set `agent.simulate-ports-taken = false` in [application.properties](src/main/resources/application.properties)


