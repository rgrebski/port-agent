package com.grebski.sample.agent.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.grebski.sample.agent.service.PortAgentService;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;
import java.util.Set;

@SpringComponent
@UIScope
@RequiredArgsConstructor
public class PortUsageModel implements Serializable {

    private static final int NUMBER_OF_EXPECTED_OPEN_PORTS = 1;
    private final PortAgentService portAgentService;

    @SuppressWarnings("unused")
    public Set<Integer> getUsedPorts() {
        return portAgentService.getUsedPorts();
    }

    @JsonIgnore
    public String getUsedPortsAsString() {
        return portAgentService.getUsedPorts().toString();
    }

    public boolean isHealthy(){
        return portAgentService.getUsedPorts().size() == NUMBER_OF_EXPECTED_OPEN_PORTS;
    }
}
