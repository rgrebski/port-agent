package com.grebski.sample.agent.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Serializable;
import java.net.ConnectException;
import java.net.Socket;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.concurrent.TimeUnit.SECONDS;

@Component
@Slf4j
@RequiredArgsConstructor
public class PortAgentService implements Serializable {

    public static final int STARTING_PORT = 3000;
    private static final int PORT_MAX = (1 << 16) - 1;

    private final Set<Integer> usedPorts = ConcurrentHashMap.newKeySet();
    private final AtomicInteger currentMaxPort = new AtomicInteger(STARTING_PORT);


    @Scheduled(fixedRate = 5, timeUnit = SECONDS)
    public void checkAndUpdatePorts() {
        for (int portNumber = currentMaxPort.get(); portNumber <= PORT_MAX; portNumber++) {
            if(!available(portNumber)){
                log.debug("Adding used port to list of used ports %s".formatted(portNumber));
                usedPorts.add(portNumber);
                currentMaxPort.incrementAndGet();
            } else {
                break;
            }
        }

        log.debug("Used app ports: %s".formatted(usedPorts));
    }



    private static boolean available(int port) throws IllegalStateException {
        try (Socket ignored = new Socket("localhost", port)) {
            return false;
        } catch (ConnectException e) {
            return true;
        } catch (IOException e) {
            throw new IllegalStateException("Error while trying to check open port", e);
        }
    }

    public Set<Integer> getUsedPorts() {
        return Set.copyOf(usedPorts);
    }
}
