package com.grebski.sample.agent.service;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static com.grebski.sample.agent.service.PortAgentService.STARTING_PORT;


@Component
@ConditionalOnProperty(name = "agent.simulate-ports-taken", havingValue = "true")
@Slf4j
public class PortUsageSimulator {

    private final AtomicInteger nextPortToTake = new AtomicInteger(STARTING_PORT);
    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    private final List<ServerSocket> openSockets = new LinkedList<>();

    @PostConstruct
    public void init(){
        log.warn("Enabled port usage simulator!");
    }

    @Scheduled(fixedRate = 10, timeUnit = TimeUnit.SECONDS, initialDelay = 0)
    public void makeNextPortTaken() {
        var currentPort = nextPortToTake.get();
        try {
            openPort(currentPort);
            log.debug("I have opened port: %s".formatted(currentPort));
            nextPortToTake.incrementAndGet();
        } catch (IOException e) {
            log.error("Cannot open port %s".formatted(currentPort), e);
        }
    }

    private void openPort(int port) throws IOException {
        ServerSocket socket = new ServerSocket(port);
        openSockets.add(socket); 
    }
}
