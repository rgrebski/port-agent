package com.grebski.sample.api;

import com.grebski.sample.agent.model.PortUsageModel;
import com.grebski.sample.agent.service.PortAgentService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class HealthEndpoint {

    private final PortAgentService portAgentService;

    @GetMapping("/health")
    public ResponseEntity<PortUsageModel> getPortUsageModel(){
        return ResponseEntity.status(200).body(new PortUsageModel(portAgentService));
    }
}
