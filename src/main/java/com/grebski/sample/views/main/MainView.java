package com.grebski.sample.views.main;

import com.grebski.sample.agent.model.PortUsageModel;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import de.codecamp.vaadin.serviceref.ServiceRef;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

@SpringComponent
@UIScope
@PageTitle("Main")
@Route(value = "")
public class MainView extends VerticalLayout {

    private final TextArea openedPortsTextField;
    private final Button statusButton;
    // we do not want to serialize spring bean
    private final ServiceRef<PortUsageModel> portUsageModel;

    @Autowired
    public MainView(ServiceRef<PortUsageModel> portUsageModel) {
        this.portUsageModel = portUsageModel;
        openedPortsTextField = initTextArea(portUsageModel.get().getUsedPortsAsString());
        statusButton = new Button("Unknown");
        statusButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY,
                ButtonVariant.LUMO_ICON);

        var horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(new Span("Status"));
        horizontalLayout.add(statusButton);


        setMargin(true);
        add(horizontalLayout, openedPortsTextField);

        updatePagePeriodically();
    }

    private TextArea initTextArea(String value) {
        final TextArea openedPortsTextField;
        openedPortsTextField = new TextArea();
        openedPortsTextField.setLabel("Opened Ports");
        openedPortsTextField.setValue(value);
        openedPortsTextField.setSizeFull();
        openedPortsTextField.setValueChangeMode(ValueChangeMode.ON_CHANGE);
        return openedPortsTextField;
    }

    private void updatePagePeriodically() {
        Flux.interval(Duration.of(1, ChronoUnit.SECONDS))
                .doOnNext(i -> getUI().ifPresent(ui -> ui.access(this::updateFields)))
                .subscribe();
    }

    private void updateFields() {
        openedPortsTextField.setValue(portUsageModel.get().getUsedPortsAsString());
        updateStatusButton(portUsageModel.get().isHealthy());

    }

    private void updateStatusButton(boolean healthy) {
        statusButton.removeThemeVariants(ButtonVariant.values());
        if (healthy) {
            statusButton.setText("Up");
            statusButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY,
                    ButtonVariant.LUMO_SUCCESS);
        } else {
            statusButton.setText("Down");
            statusButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY,
                    ButtonVariant.LUMO_ERROR);
        }
    }

    @Override
    protected void onDetach(DetachEvent detachEvent) {
        // do nothing
    }
}
